module.exports = function(RED) 
{
    function Encrypt(config) 
    {
        let XMLHttpRequest = require('xhr2');
        let parsecurl = require('parse-curl');

        RED.nodes.createNode(this,config);
        var node = this;
        var curl;

        node.on('input', function(msg) 
        {
			let xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() 
            {
                if(this.readyState == 4)
                {
                    var result = 
                    {
                        response: {},
                        request: {}
                    };
                    
                    val = this.response;

                    try
                    {
                        val = JSON.parse(val);
                    }
                    catch
                    {
                        
                    }

                    result.response.body = val;
                    result.response.status = {code: this.status, text: this.statusText};
                    result.response.headers = this._responseHeaders;

                    result.request.URL = this.responseURL;
                    result.request.method = this._method;
                    result.request.headers = this._headers;
                    result.curl = curl;

                    msg.payload = result;
                
                    node.send(msg);
                }
            }

            curl = parsecurl(msg.payload + " -v");

            xhr.open(curl.method.toUpperCase(), curl.url);

            for (const [key, value] of Object.entries(curl.header)) 
            {
                xhr.setRequestHeader(key, value);
            }

            xhr.send(curl.body);
        });
    }

    RED.nodes.registerType("sense-curl", Encrypt);
}